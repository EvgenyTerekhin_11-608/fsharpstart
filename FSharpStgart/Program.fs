﻿open System

type Result<'TS> =
    | Success of 'TS
    | Failed of string



type Parcer<'T> = Parcer of (string -> Result<'T * string>)

let run a str =
        let (Parcer func) = a;
        func str



let pchar charMatch =
        let innerFn str =
            if str = "" || str = null then
                Failed ""
            else if str.[0] = charMatch then
                let remaining = str.[1..]
                Success(charMatch, remaining)
            else
                Failed "expecting"
        Parcer innerFn





let parcer1 = pchar 'A'
let parcer2 = pchar 'B'
let parcer3 = pchar 'C'
let parcer4 = pchar 'D'
let parcer5 = pchar 'E'

let andThen parser1 parser2 =
      let innerFn input =
                let result1 = run parser1 input
                match result1 with
                | Failed err ->
                    Failed err
                | Success(value1, remaining1) ->
                    let result2 = run parser2 remaining1
                    match result2 with
                    | Failed err ->
                        Failed err
                    | Success(value2, remaining2) ->
                        let newValue = (value1, value2)
                        Success(newValue, remaining2)

      Parcer innerFn


let orElse parser1 parser2 =
        let innerFn input =
            // run parser1 with the input
            let result1 = run parser1 input

            // test the result for Failure/Success
            match result1 with
            | Success result ->
                // if success, return the original result
                result1

            | Failed err ->
                // if failed, run parser2 with the input
                let result2 = run parser2 input

                // return parser2's result
                result2

        // return the inner function
        Parcer innerFn

let (.>>.) = andThen;
let parcer = parcer1 .>>. parcer2 .>>. parcer3 .>>. parcer4 .>>. parcer5
let (|>>|) = orElse;
let parsers = [ parcer1; parcer2; parcer3; parcer4; parcer5 ]

let s = Seq.reduce (|>>|) parsers


let anyOf list =
    list |> List.map pchar
         |> (List.reduce (|>>|))

let parseLowercase =
    anyOf [ 'a'..'z' ]

let parseDigit =
    anyOf [ '0'..'9' ]




let str = "svsvbsjlfnvdoflkmfsvd1232312"
let str1 = "sccsasacascassc";


let mapP f parser =
    let innerFn input =
        // run parser with the input
        let result = run parser input

        // test the result for Failure/Success
        match result with
        | Success(value, remaining) ->
            // if success, return the value transformed by f
            let newValue = f value
            Success(newValue, remaining)

        | Failed err ->
            // if failed, return the error
            Failed err
    // return the inner function
    Parcer innerFn




let returnP x =
    let innerF input =
        Success(x, input)
    Parcer innerF

let applyP Fx Px =
    let s = Fx .>>. Px
    s |> mapP (fun (f, x) -> f x)

let (<*>) = applyP


let lift2 f xP yP =
    returnP f <*> xP <*> yP

let lift3 f xP yP =
    returnP (f <*> xP <*> yP)

let add = (+)
let addP = lift2 (+)


let startsWith (str : string) (prefix : char) =
    str.StartsWith(prefix)

let startsWithP =
    lift2 startsWith


let helper1 parser1 parser2 =
    let innerFn input =
        let result1 = run parser1 input
        match result1 with
        | Failed f ->
            Failed f
        | Success(value1, remaining1) ->
            let result2 = run parser2 remaining1
            match result2 with
            | Failed f1 ->
                Failed f1
            | Success(value2, remaining2) ->
                Success((value1, value2), remaining2)
    Parcer innerFn

let helper2 f parser1 parser2 =
    let innerFn input =
        let result1 = run parser1 input
        match result1 with
        | Failed f ->
            Failed f
        | Success(value1, remaining1) ->
            let result2 = run parser2 remaining1
            match result2 with
            | Failed f1 ->
                Failed f1
            | Success(value2, remaining2) ->
                let newValue = f (value1, value2)
                Success(newValue, remaining2)
    Parcer innerFn


let helper3 p1 p2 p3 = helper2 (fun (x, (y, z)) -> (x, y, z)) p1 (helper1 p2 p3)

let helper0 value =
    let innerFn input =
        Success(value, input)
    Parcer innerFn;


let rec sequence parseList =
    let glueFn head tail = head :: tail
    let helper4 headParser tailParsers = helper2 (fun (x, y) -> (x :: y)) headParser tailParsers
    match parseList with
    | [] -> helper0 []
    | head :: tail -> helper4 head (sequence tail)


run (sequence parsers) "ABCDwE" |> printf "%A"


let charListToStr charList =
     String(List.toArray charList)

// match a specific string
let rec many parcer str =
        let result = run parcer str;
        match result with
        | Success(sub, rem) ->
            let (sub1, rem1) = many parcer rem;
            (Array.concat [ sub1; sub ], rem1)
        | Failed f -> ([||], str)


let parseABC = many (sequence [parcer1])

run parseABC "ABCDE" |> printf "%A /n" // Success ("ABC", "DE")
run parseABC "A|CDE" |> printf "%A" // Failure "Expecting 'B'. Got '|'"
run parseABC "AB|DE" |> printf "%A" // Failure "Expecting 'C'. Got '|'"

